﻿using Serializer.DTO;
using System.Collections.Generic;
using Xunit;

namespace Application.Test
{
    public class ApplicationCommandTest
    {
        public static IEnumerable<object[]> DataForApplicationErrorTests => new List<object[]>()
                {
                    new object[] {
                        new ApplicationError() { Message = "Error", Code = 404 },
                        false,
                    },
                    new object[] {
                        new ApplicationError(),
                        false,
                    },
                    new object[] {
                        // Passing null explicitly
                        null,
                        true
                    },
                    new object[] {
                        // Passing default value
                        new ApplicationMessageCommand().Error,
                        true
                    }
                };
        [Theory, MemberData(nameof(DataForApplicationErrorTests))]
        public void Succes_ShouldCheckForErrors(ApplicationError error, bool expectedValue)
        {
            var appmsg = new ApplicationMessageCommand() { Error = error };
            Assert.Equal(expectedValue, appmsg.Success);
        }

    }
}
