﻿using Application.Facade;
using Domain.Services.NotificationService;
using Moq;
using Serializer.DTO;
using System;
using Xunit;

namespace Application.Test
{
    public class ApplicationFacadeTest
    {
        private Mock<ICommandHandler> commandHandlerMock;
        private Mock<INotificationService> notificationServiceMock;
        private IApplicationFacade applicationFacade;

        public ApplicationFacadeTest()
        {
            commandHandlerMock = new Mock<ICommandHandler>();
            notificationServiceMock = new Mock<INotificationService>();
            applicationFacade = new ApplicationFacade(commandHandlerMock.Object, notificationServiceMock.Object);
        }

        [Fact]
        public void SendMessage_ShoudCallHandleMessage()
        {
            var command = new ClientCommand()
            {
                Nickname = "john",
                Command = new SendMessageCommand()
                {
                    Action = MessageAction.CREATE_USER,
                    Payload = new SendMessagePayload()
                    {
                        Destination = "bob"
                    },
                }
            };

            Guid clientId = Guid.NewGuid();
            applicationFacade.SendMessage(clientId, command);

            commandHandlerMock.Verify(
                (m) => m.HandleMessage("john", command.Command, clientId));
        }

        [Fact]
        public void SendMessage_ShoudCallSendErrorWhenExceptionThrown()
        {
            var command = new ClientCommand()
            {
                Nickname = "john",
                Command = new SendMessageCommand()
                {
                    Action = MessageAction.CREATE_USER,
                    Payload = new SendMessagePayload()
                    {
                        Destination = "bob"
                    },
                }
            };
            commandHandlerMock.Setup(
                (m) => m.HandleMessage(
                    It.IsAny<string>(),
                    It.IsAny<SendMessageCommand>(),
                    It.IsAny<Guid>())).Throws(new Exception("message"));

            Guid clientId = Guid.NewGuid();
            applicationFacade.SendMessage(clientId, command);

            notificationServiceMock.Verify(
                (m) => m.SendError(It.IsAny<Exception>(), clientId));
        }
    }
}
