using Application.Facade;
using Serializer.DTO;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Application.Test
{
    public class ChatApplicationTest
    {
        private readonly IApplicationFacade chatApplication;

        public ChatApplicationTest()
        {
            this.chatApplication = new ApplicationFacade();
        }
        [Fact]
        public void AddUser_ShouldCallSubscription()
        {
            var clientId = Guid.NewGuid();
            var numOfCalls = 0;
            Func<ICommandTypeMatcher, Task> callback = (message) =>
            {
                numOfCalls++;
                return Task.CompletedTask;
            };
            this.chatApplication.Subscribe(clientId, callback);
            this.chatApplication.SendMessage(clientId,
            new ClientCommand()
            {
                Command = new SendMessageCommand()
                {
                    Action = MessageAction.CREATE_USER
                },
                Nickname = "john"
            });
            Assert.Equal(1, numOfCalls);
        }

        [Fact]
        public void Subscribe_ShouldCallSubscriptionWhenSendPublicMessage()
        {
            var clientId = Guid.NewGuid();
            //TODO: Melhorar mocks
            var numOfCalls = 0;
            Func<ICommandTypeMatcher, Task> callback = (message) =>
            {
                numOfCalls++;
                return Task.CompletedTask;
            };
            this.chatApplication.Subscribe(clientId, callback);
            this.chatApplication.SendMessage(clientId,
            new ClientCommand()
            {
                Command = new SendMessageCommand()
                {
                    Action = MessageAction.CREATE_USER
                },
                Nickname = "john"
            });
            this.chatApplication.SendMessage(clientId,
            new ClientCommand()
            {
                Command = new SendMessageCommand()
                {
                    Payload = new SendMessagePayload() { Text = "msg" },
                    Action = MessageAction.SEND_TEXT
                },
                Nickname = "john"
            });
            Assert.Equal(2, numOfCalls);
        }

        [Fact]
        public void Subscribe_ShouldCallSubscriptionWhenSendPrivateMessage()
        {
            var johnId = Guid.NewGuid();
            //TODO: Melhorar mocks
            var numOfCalls = 0;
            Func<ICommandTypeMatcher, Task> callback = (message) =>
            {
                numOfCalls++;
                return Task.CompletedTask;
            };
            RegisterUser(johnId, (n) => Task.CompletedTask, "john");
            RegisterUser(Guid.NewGuid(), callback, "bob");
            this.chatApplication.SendMessage(johnId,
            new ClientCommand()
            {
                Command = new SendMessageCommand()
                {
                    Payload = new SendMessagePayload() { Text = "msg", Destination = "bob" },
                    Action = MessageAction.SEND_TEXT
                },
                Nickname = "john"
            });
            Assert.Equal(2, numOfCalls);
        }

        [Fact]
        public void Subscribe_ShouldNotCallSubscriptionWhenSendPrivateMessageToAnotherUser()
        {
            var johnId = Guid.NewGuid();
            //TODO: Melhorar mocks
            var numOfCalls = 0;
            Func<ICommandTypeMatcher, Task> callback = (message) =>
            {
                numOfCalls++;
                return Task.CompletedTask;
            };
            RegisterUser(johnId, (n) => Task.CompletedTask, "john");
            RegisterUser(Guid.NewGuid(), callback, "bob");
            this.chatApplication.SendMessage(johnId,
            new ClientCommand()
            {
                Command = new SendMessageCommand()
                {
                    Payload = new SendMessagePayload() { Text = "msg", Destination = "john" },
                    Action = MessageAction.SEND_TEXT
                },
                Nickname = "bob"
            });
            Assert.Equal(1, numOfCalls);
        }

        [Fact]
        public void Exit_ShouldSendNotificationToUser()
        {
            var johnId = Guid.NewGuid();
            var numOfCalls = 0;
            Func<ICommandTypeMatcher, Task> callback = (message) =>
            {
                if(message is UserRemovedCommand) numOfCalls++;
                return Task.CompletedTask;
            };
            RegisterUser(johnId, callback, "john");
            this.chatApplication.SendMessage(johnId,
                new ClientCommand()
                {
                    Command = new SendMessageCommand()
                    {
                        Action = MessageAction.EXIT
                    },
                    Nickname = "john"
                });
            Assert.Equal(1, numOfCalls);
        }

        private void RegisterUser(Guid clientId, Func<ICommandTypeMatcher, Task> callback, string nickname)
        {
            this.chatApplication.Subscribe(clientId, callback);
            this.chatApplication.SendMessage(clientId,
            new ClientCommand()
            {
                Command = new SendMessageCommand()
                {
                    Action = MessageAction.CREATE_USER
                },
                Nickname = nickname
            });
        }

    }
}
