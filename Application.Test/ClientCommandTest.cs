﻿using Serializer.DTO;
using System.Collections.Generic;
using Xunit;

namespace Application.Test
{
    public class ClientCommandTest
    {
        public static IEnumerable<object[]> DataForApplicationErrorTests => new List<object[]>()
                {
                    new object[] {
                        "8256B13A-E49E-435D-BF4D-DE550B1FCFC2",
                        false,
                    },
                    new object[] {
                        // Passing null explicitly
                        null,
                        true
                    },
                    new object[] {
                        // Passing default value (payload)
                        new SendMessageCommand().Payload?.Destination,
                        true
                    },
                    new object[] {
                        // Passing default value (destination)
                        new SendMessageCommand() { Payload = new SendMessagePayload() }.Payload.Destination,
                        true
                    }
                };
        [Theory, MemberData(nameof(DataForApplicationErrorTests))]
        public void Public_ShouldCheckForChannel(string channel, bool expectedValue)
        {
            var appmsg = new SendMessageCommand() { Payload = new SendMessagePayload() { Destination = channel } };
            Assert.Equal(expectedValue, appmsg.Public);
        }

    }
}
