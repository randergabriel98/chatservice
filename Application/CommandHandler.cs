﻿using Domain.Services.MessageService;
using Domain.Services.UserService;
using Serializer.DTO;
using System;

namespace Application
{
    public class CommandHandler : ICommandHandler
    {
        private readonly IUserService userService;
        private readonly IMessageService messageService;

        public CommandHandler(IMessageService messageService, IUserService userService)
        {
            this.messageService = messageService;
            this.userService = userService;
        }

        public void HandleMessage(string userNickname, SendMessageCommand message, Guid clientId)
        {
            switch (message.Action)
            {
                case MessageAction.CREATE_USER:
                    userService.CreateUser(userNickname, clientId);
                    break;
                case MessageAction.SEND_TEXT:
                    {
                        messageService.SendMessage(userNickname, message.Payload.Text, clientId, message.Payload.Destination, message.Payload.Channel);
                        break;
                    }
                case MessageAction.CREATE_CHANNEL:
                    {
                        messageService.CreateChannel(message.Payload.Text, userNickname, clientId);
                        break;
                    }
                case MessageAction.JOIN_CHANNEL:
                    {
                        messageService.JoinChannel(message.Payload.Text, userNickname, clientId);
                        break;
                    }
                case MessageAction.EXIT:
                    userService.RemoveUser(userNickname, clientId);
                    break;
            }
        }
    }
}
