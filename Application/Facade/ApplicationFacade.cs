﻿using Domain.Model;
using Domain.Services.MessageService;
using Domain.Services.NotificationService;
using Domain.Services.UserService;
using Serializer.DTO;
using System;
using System.Threading.Tasks;

namespace Application.Facade
{
    public class ApplicationFacade : IApplicationFacade
    {
        private readonly ICommandHandler messageHandler;
        private readonly INotificationService notificationService;
        public ApplicationFacade()
        {
            //TODO: Mover para DI
            // Injetei as dependências explicitamente aqui, pois não
            // era permitido o uso de ferramentas externas. Criei um segundo construto
            this.notificationService = new NotificationService();
            var userService = new UserService(this.notificationService);
            this.messageHandler = new CommandHandler(
                    new MessageService(this.notificationService, userService),
                    userService
                );
        }

        public ApplicationFacade(ICommandHandler messageHandler, INotificationService notificationService)
        {
            this.messageHandler = messageHandler;
            this.notificationService = notificationService;
        }

        public void SendMessage(Guid clientId, ClientCommand message)
        {
            //TODO: Tratar erros
            try
            {
                messageHandler.HandleMessage(message.Nickname, message.Command, clientId);
            }
            catch (Exception ex)
            {
                notificationService.SendError(ex, clientId);
            }
        }

        public void Subscribe(Guid clientId, Func<ICommandTypeMatcher, Task> callback)
        {
            notificationService.RegisterListener(clientId,
                async (notification) =>
                {
                    switch (notification.Type)
                    {
                        case Notification.NotificationType.TEXT_MESSAGE:
                            {
                                await callback(new ApplicationMessageCommand()
                                {
                                    Error = notification.HasError ?
                                        new ApplicationError() { Message = notification.Error } : null,
                                    Payload = new ApplicationPayload()
                                    {
                                        Text = notification.Text,
                                        Sender = notification.Author?.Nickname,
                                        Private = notification.Private,
                                        Channel = notification.Channel
                                    }
                                });
                                break;
                            }
                        case Notification.NotificationType.USER_CREATED:
                            await callback(new UserCreatedCommand() { Payload = new UserCreatedPayload() { } });
                            break;
                        case Notification.NotificationType.LISTENER_REMOVED:
                            await callback(new UserRemovedCommand() { Payload = new UserRemovedPayload() });
                            break;
                    }

                });
        }
    }
}
