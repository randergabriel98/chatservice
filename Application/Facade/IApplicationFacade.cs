﻿using Serializer.DTO;
using System;
using System.Threading.Tasks;

namespace Application.Facade
{
    public interface IApplicationFacade
    {
        void SendMessage(Guid clientId, ClientCommand message);
        void Subscribe(Guid clientId, Func<ICommandTypeMatcher, Task> callback);
    }
}