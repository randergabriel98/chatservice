﻿using Serializer.DTO;
using System;

namespace Application
{
    public interface ICommandHandler
    {
        void HandleMessage(string userNickname, SendMessageCommand message, Guid clientId);
    }
}