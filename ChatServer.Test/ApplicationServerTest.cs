using Application.Facade;
using Moq;
using Serializer;
using Serializer.DTO;
using System;
using System.Threading.Tasks;
using Xunit;

namespace ChatServer.Test
{
    public class ApplicationServerTest
    {
        private readonly ApplicationServer applicationServer;
        private readonly Mock<INetworkServerAdapter> mockNetAdapter;
        private readonly Mock<IApplicationFacade> mockFacade;

        public ApplicationServerTest()
        {
            mockNetAdapter = new Mock<INetworkServerAdapter>();
            mockFacade = new Mock<IApplicationFacade>();
            this.applicationServer = new ApplicationServer(mockFacade.Object, mockNetAdapter.Object);
        }

        [Fact]
        public async void Start_ShouldCallAdapterInit()
        {
            await applicationServer.Start();
            mockNetAdapter.Verify(m => m.Init());
        }

        [Fact]
        public async void Start_ShouldCallAdapterLoop()
        {
            await applicationServer.Start();
            mockNetAdapter.Verify(m => m.Loop(It.IsAny<Func<Guid, Task>>()));
        }

        [Fact]
        public async void InitNewConnection_ShouldCallFacadeSubscribe()
        {
            mockNetAdapter.Setup(m => m.IsConnected(It.IsAny<Guid>())).Returns(false);
            await applicationServer.InitNewConnection(Guid.NewGuid());
            mockFacade.Verify(m => m.Subscribe(It.IsAny<Guid>(), It.IsAny<Func<ICommandTypeMatcher, Task>>()));
        }

        [Fact]
        public async void ReadMessage_ShouldCallReadFromClient()
        {
            mockNetAdapter.Setup(m => m.ReadFromClient(It.IsAny<Guid>())).Returns(
                Task.FromResult(
                    SerializeHelper.SerializeString<ClientCommand>(
                        new ClientCommand()
                        )
                    )
                );

            await applicationServer.ReadMessage(Guid.NewGuid());
            mockNetAdapter.Verify(m => m.ReadFromClient(It.IsAny<Guid>()));
        }

        [Fact]
        public async void ReadMessage_ShouldCallSendMessage()
        {
            mockNetAdapter.Setup(m => m.ReadFromClient(It.IsAny<Guid>())).Returns(
                Task.FromResult(
                    SerializeHelper.SerializeString<ClientCommand>(
                        new ClientCommand()
                        )
                    )
                );
            await applicationServer.ReadMessage(Guid.NewGuid());
            mockFacade.Verify(m => m.SendMessage(It.IsAny<Guid>(), It.IsAny<ClientCommand>()));
        }

        [Fact]
        public async void SendMessage_ShouldCallSendMessageToClient()
        {
            await applicationServer.GenerateSendMessageFunc(Guid.NewGuid())(new ApplicationMessageCommand());
            mockNetAdapter.Verify(m => m.SendMessageToClient(It.IsAny<Guid>(), It.IsAny<string>()));
        }

        [Fact]
        public void Dispose_ShouldCallAdapterDispose()
        {
            applicationServer.Dispose();
            mockNetAdapter.Verify(m => m.Dispose());
        }
    }
}
