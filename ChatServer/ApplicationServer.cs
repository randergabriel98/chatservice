﻿using Application.Facade;
using Serializer;
using Serializer.DTO;
using System;
using System.Threading.Tasks;

namespace ChatServer
{
    public class ApplicationServer : IDisposable
    {
        private readonly IApplicationFacade applicationFacade;

        private readonly INetworkServerAdapter networkAdapter;

        public ApplicationServer(IApplicationFacade applicationFacade, INetworkServerAdapter networkAdapter)
        {
            this.applicationFacade = applicationFacade;
            this.networkAdapter = networkAdapter;
        }

        public void Dispose()
        {
            this.networkAdapter.Dispose();
        }

        public async Task Start()
        {
            this.networkAdapter.Init();
            await this.networkAdapter.Loop(async (guid) => await InitNewConnection(guid));
        }

        public async Task InitNewConnection(Guid clientId)
        {

            applicationFacade.Subscribe(clientId, GenerateSendMessageFunc(clientId));
            while (networkAdapter.IsConnected(clientId))
            {
                try
                {
                    await ReadMessage(clientId);
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Read Message failed with message: {ex.Message}");
                }
            }
        }

        public async Task ReadMessage(Guid clientId)
        {
            string rawCommand = await networkAdapter.ReadFromClient(clientId);
            var command = SerializeHelper.DeserializeString<ClientCommand>(rawCommand);
            Console.WriteLine("Deserialized");
            applicationFacade.SendMessage(clientId, command);
        }

        public Func<ICommandTypeMatcher, Task> GenerateSendMessageFunc(Guid clientId)
        {
            return async (command) =>
            {
                await networkAdapter.SendMessageToClient(clientId, SerializeHelper.SerializeCommand(command));
            };
        }
    }
}
