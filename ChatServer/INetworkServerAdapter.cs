﻿using System;
using System.Threading.Tasks;

namespace ChatServer
{
    public interface INetworkServerAdapter : IDisposable
    {
        void Init();
        Task Loop(Func<Guid, Task> p);
        Task SendMessageToClient(Guid clientId, string message);
        bool IsConnected(Guid clientId);
        Task<string> ReadFromClient(Guid clientId);
    }
}