﻿using Application.Facade;
using System;
using System.Threading.Tasks;

namespace ChatServer
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello World, from server!");
            using (var server = new ApplicationServer(new ApplicationFacade(), new WebSocketServerAdapter()))
            {
                await server.Start();
            }
        }
    }
}
