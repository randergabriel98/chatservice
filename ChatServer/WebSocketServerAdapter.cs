﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ChatServer
{
    public class WebSocketServerAdapter : INetworkServerAdapter
    {
        public HttpListener httpListener;
        public Dictionary<Guid, WebSocket> keyValuePairs = new Dictionary<Guid, WebSocket>();

        public void Init()
        {
            httpListener = InitializeListener();
        }

        public async Task<string> ReadFromClient(Guid clientId)
        {
            var webSocket = GetWebSocketForClientId(clientId);
            //Busca informação do cliente
            ArraySegment<byte> bytesReceived = new ArraySegment<byte>(new byte[1024]);
            var result = await webSocket.ReceiveAsync(bytesReceived, CancellationToken.None);
            return Encoding.UTF8.GetString(bytesReceived.ToArray(), 0, result.Count);
        }

        public async Task SendMessageToClient(Guid clientId, string messageToClient)
        {
            WebSocket webSocket = GetWebSocketForClientId(clientId);
            await webSocket.SendAsync(Encoding.UTF8.GetBytes(messageToClient), WebSocketMessageType.Text, true, CancellationToken.None);
        }

        private WebSocket GetWebSocketForClientId(Guid clientId)
        {
            return keyValuePairs[clientId];
        }

        public bool IsConnected(Guid clientId)
        {
            return GetWebSocketForClientId(clientId).State == WebSocketState.Open;
        }

        public static HttpListener InitializeListener()
        {
            // Cria o HTTP Listener, define o endpoint do servidor
            HttpListener httpListener = new HttpListener();
            httpListener.Prefixes.Add("http://localhost:7000/");
            httpListener.Start();
            return httpListener;
        }

        private async Task OpenConnection(HttpListenerWebSocketContext webSocketContext, Func<Guid, Task> notifyNewConnection)
        {
            WebSocket webSocket = webSocketContext.WebSocket;
            var clientId = Guid.NewGuid();
            keyValuePairs.Add(clientId, webSocket);
            await notifyNewConnection(clientId);
        }

        public async Task Loop(Func<Guid, Task> notifyNewConnection)
        {
            var clientId = Guid.NewGuid();
            while (true)
            {
                HttpListenerContext context = await httpListener.GetContextAsync();
                if (context.Request.IsWebSocketRequest)
                {
                    HttpListenerWebSocketContext webSocketContext = await context.AcceptWebSocketAsync(null);
                    new Thread(async () =>
                    {
                        //TODO: Tratar reconexão e erro
                        await OpenConnection(webSocketContext, notifyNewConnection);
                    }).Start();
                }
            }
        }

        public void Dispose()
        {
            this.httpListener.Close();
        }
    }
}