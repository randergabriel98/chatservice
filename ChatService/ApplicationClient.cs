﻿using Serializer;
using Serializer.DTO;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ChatService
{
    public class ApplicationClient
    {
        private bool created = false;
        private bool connected = true;
        private WebSocketClient webSocketClient;

        private static ClientCommand ParseCommand(string nickname, string msg)
        {
            var exit = msg.StartsWith("/exit");
            var privateMessage = msg.StartsWith("/p ");
            var helpMessage = msg.StartsWith("/help");
            var createChannel = msg.StartsWith("/create");
            var sendMessageToChannel = msg.StartsWith("/channel ");
            var joinChannel = msg.StartsWith("/join ");
            if (helpMessage)
            {
                Console.Write(File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "help-text.txt")));
                return null;
            }
            //TODO: Tratamento para nickname com espaco
            var text = privateMessage ? string.Join(" ", msg.Split(" ").Skip(2)) : msg;
            var destination = privateMessage ? msg.Split(" ")[1] : null;
            string channel = null;
            MessageAction messageAction = exit ? MessageAction.EXIT : MessageAction.SEND_TEXT;
            if(createChannel)
            {
                messageAction = MessageAction.CREATE_CHANNEL;
                text = string.Join(" ", msg.Split(" ").Skip(1));
            }
            if(sendMessageToChannel)
            {
                text = string.Join(" ", msg.Split(" ").Skip(2));
                channel = msg.Split(" ").Skip(1).ToArray()[0];
            }
            if(joinChannel)
            {
                text = msg.Split(" ").Skip(1).ToArray()[0];
                messageAction = MessageAction.JOIN_CHANNEL;
            }
            ClientCommand command = new ClientCommand()
            {
                Nickname = nickname,
                Command = new SendMessageCommand()
                {
                    Action = messageAction,
                    Payload = new SendMessagePayload()
                    {
                        Text = text,
                        Destination = destination,
                        Channel = channel
                    }
                }
            };
            return command;
        }

        private async Task SendChatMessage(string nickname)
        {
            ArraySegment<byte> commandStr;
            Thread.Sleep(100);
            if (!this.connected) return;
            Console.WriteLine("Insert your message (/help for help)");
            var msg = Console.ReadLine();
            if (this.connected)
            {
                ClientCommand command = ParseCommand(nickname, msg);
                if (command != null)
                {
                    commandStr = SerializeHelper.SerializeBytes(command);

                    await webSocketClient.SendMessage(commandStr);
                }
            }
        }

        private async Task<string> ChooseNickname()
        {
            string nickname = null;
            while (!created)
            {
                ArraySegment<byte> createCommandStr;
                Console.WriteLine("Insert your username");
                nickname = Console.ReadLine();
                var createCommand = new ClientCommand()
                {
                    Nickname = nickname,
                    Command = new SendMessageCommand()
                    {
                        Action = MessageAction.CREATE_USER,
                    }
                };
                createCommandStr = SerializeHelper.SerializeBytes(createCommand);
                await webSocketClient.SendMessage(createCommandStr);
                ICommandTypeMatcher message = await webSocketClient.ReadMessage();
                HandleCommand(message);
            }

            return nickname;
        }

        public async Task DoClientWebSocket()
        {
            this.connected = true;
            //TODO: Refatorar cliente!!
            Thread.Sleep(500);
            using (webSocketClient = new WebSocketClient())
            {
                await webSocketClient.Connect();
                string nickname = await ChooseNickname();
                new Task(async () =>
                {
                    await ReadMessageLoop();
                }).Start();
                while (webSocketClient.IsConnected() && this.connected)
                {
                    await SendChatMessage(nickname);
                }
            }
        }



        private void HandleCommand(ICommandTypeMatcher command)
        {
            if (command is ApplicationMessageCommand)
            {
                var parsedCommand = command as ApplicationMessageCommand;
                HandleCommand(parsedCommand);
            }
            else if (command is UserCreatedCommand)
            {
                var parsedCommand = command as UserCreatedCommand;
                HandleCommand(parsedCommand);
            }
            else if (command is UserRemovedCommand)
            {
                var parsedCommand = command as UserRemovedCommand;
                HandleCommand(parsedCommand);
            }
        }

        private void Disconnect()
        {
            this.connected = false;
        }

        private void HandleCommand(UserRemovedCommand command)
        {
            this.Disconnect();
        }

        private void HandleCommand(UserCreatedCommand command)
        {
            this.created = true;
        }

        private void HandleCommand(ApplicationMessageCommand parsedCommand)
        {
            if (!parsedCommand.Success)
            {
                Console.WriteLine($"ops! an error ocurried. Message: \n\t{parsedCommand.Error?.Message}");
            }

            else
            {
                Console.WriteLine($"{parsedCommand.Payload.Sender} says:\n\t{parsedCommand.Payload.Text} {(parsedCommand.Payload.Private ? "(privately)" : "")}{(parsedCommand.Payload.Channel != null ? "\nin channel:\n\t" + parsedCommand.Payload.Channel : "")}");
            }
        }

        private async Task ReadMessageLoop()
        {
            while (connected)
            {
                ICommandTypeMatcher response = await webSocketClient.ReadMessage();
                HandleCommand(response);
            }
        }
    }
}