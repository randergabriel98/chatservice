﻿using System;

namespace ChatService
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World, from client!");
            while (true)
            {
                new ApplicationClient().DoClientWebSocket().Wait();
                Console.WriteLine("Do you want to start a new session? (yes for start new session, anything else to quit)");
                var runAgain = Console.ReadLine();
                if(runAgain != "yes")
                {
                    break;
                }
            }
            Console.WriteLine("Bye bye!");
        }
    }
}
