﻿using Serializer;
using Serializer.DTO;
using System;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;

namespace ChatService
{
    public class WebSocketClient : IDisposable
    {
        private ClientWebSocket webSocketClient;
        public async Task Connect()
        {
            webSocketClient = new ClientWebSocket();
            Uri serverUri = new Uri("ws://localhost:7000");
            await webSocketClient.ConnectAsync(serverUri, CancellationToken.None);
        }

        public void Dispose()
        {
            webSocketClient.Dispose();
        }

        public bool IsConnected()
        {
            return webSocketClient.State == WebSocketState.Open;
        }

        public async Task<ICommandTypeMatcher> ReadMessage()
        {
            ArraySegment<byte> bytesReceived = new ArraySegment<byte>(new byte[1024]);
            WebSocketReceiveResult result = await webSocketClient.ReceiveAsync(bytesReceived, CancellationToken.None);
            var response = SerializeHelper.DeserializeCommand(bytesReceived, result.Count);
            return response;
        }

        public async Task SendMessage(ArraySegment<byte> commandStr)
        {
            await webSocketClient.SendAsync(commandStr, WebSocketMessageType.Text, true, CancellationToken.None);
        }
    }
}