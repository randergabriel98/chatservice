﻿using Domain.Model;
using Domain.Services.MessageService;
using Domain.Services.NotificationService;
using Domain.Services.UserService;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Domain.Test
{
    public class MessageServiceTest
    {
        private readonly IUserService userService;
        private readonly IMessageService messageService;
        private readonly INotificationService notificationService;
        private readonly Mock<INotificationService> notificationServiceMock;
        //TODO: Usar DI nos testes
        public MessageServiceTest()
        {
            notificationServiceMock = new Mock<INotificationService>();
            notificationService = notificationServiceMock.Object;
            userService = new UserService(notificationService);
            messageService = new MessageService(notificationService, userService);
        }

        [Fact]
        public void CreatePublicMessageAddMessageToList()
        {
            var guid = Guid.NewGuid();
            var user = userService.CreateUser("john doe", guid);
            const string text = "ping";
            messageService.SendPublicMessage(user, text);
            IEnumerable<Message> createdMessages = messageService.GetAllMessages();
            var message = createdMessages.First();
            Assert.Equal(user, message.Notification.Author);
            Assert.Equal(text, message.Notification.Text);
            Assert.Equal("Public", message.Channel.Name);
        }

        [Fact]
        public void CreatePrivateMessageAddMessageToList()
        {
            var guid = Guid.NewGuid();
            var john = userService.CreateUser("john doe", guid);
            var jane = userService.CreateUser("jane doe", Guid.NewGuid());
            const string text = "ping";
            messageService.SendPrivateMessage(john, text, jane);
            IEnumerable<Message> createdMessages = messageService.GetAllMessages();
            var message = createdMessages.First();
            Assert.Equal(john, message.Notification.Author);
            Assert.Equal(text, message.Notification.Text);
        }

        [Fact]
        public void GetMessagesForUser_ReturnsPublicMessages()
        {
            var john = userService.CreateUser("john doe", Guid.NewGuid());
            var jane = userService.CreateUser("jane doe", Guid.NewGuid());
            const string text = "ping";
            messageService.SendPublicMessage(john, text);
            IEnumerable<Message> messages = messageService.GetMessagesForUser(jane);
            Assert.Single(messages);
        }

        [Fact]
        public void GetMessagesForUser_ReturnsPrivateMessages()
        {
            var john = userService.CreateUser("john doe", Guid.NewGuid());
            var jane = userService.CreateUser("jane doe", Guid.NewGuid());
            const string text = "ping";
            messageService.SendPrivateMessage(john, text, jane);
            IEnumerable<Message> messages = messageService.GetMessagesForUser(jane);
            Assert.Single(messages);
        }

        [Fact]
        public void GetMessagesForUser_ReturnsPrivateAndPublicMessages()
        {
            var john = userService.CreateUser("john doe", Guid.NewGuid());
            var jane = userService.CreateUser("jane doe", Guid.NewGuid());
            const string text = "ping";
            messageService.SendPrivateMessage(john, text, jane);
            messageService.SendPublicMessage(john, text);
            IEnumerable<Message> messages = messageService.GetMessagesForUser(jane);
            Assert.Equal(2, messages.Count());
        }

        [Fact]
        public void GetMessagesForUser_DontReturnMessagesForOthers()
        {
            var john = userService.CreateUser("john doe", Guid.NewGuid());
            var jane = userService.CreateUser("jane doe", Guid.NewGuid());
            const string text = "ping";
            messageService.SendPrivateMessage(john, text, jane);
            IEnumerable<Message> messages = messageService.GetMessagesForUser(john);
            Assert.Empty(messages);
        }

        [Fact]
        public void SendMessage_ShouldSendPublicMessage()
        {
            Guid jhonId = Guid.NewGuid();
            userService.CreateUser("john", jhonId);
            userService.CreateUser("bob", Guid.NewGuid());
            notificationServiceMock.Setup((m) => m.GetUserNicknameForClientId(jhonId)).Returns("john");

            messageService.SendMessage("john", "text", jhonId, null, null);
            var messages = messageService.GetMessagesForUser(userService.GetUser("bob"));
            Assert.Single(messages);
        }

        [Fact]
        public void SendMessage_ShouldSendPrivateMessage()
        {
            Guid jhonId = Guid.NewGuid();
            userService.CreateUser("john", jhonId);
            userService.CreateUser("bob", Guid.NewGuid());
            notificationServiceMock.Setup((m) => m.GetUserNicknameForClientId(jhonId)).Returns("john");
            messageService.SendMessage("john", "text", jhonId, "bob", null);
            var messages = messageService.GetMessagesForUser(userService.GetUser("bob"));
            Assert.Single(messages);
        }

        [Fact]
        public void SendMessage_ShouldntSendPrivateMessageForOtherUser()
        {
            Guid jhonId = Guid.NewGuid();
            userService.CreateUser("john", jhonId);
            userService.CreateUser("bob", Guid.NewGuid());
            notificationServiceMock.Setup((m) => m.GetUserNicknameForClientId(jhonId)).Returns("john");
            messageService.SendMessage("john", "text", jhonId, "bob", null);
            var messages = messageService.GetMessagesForUser(userService.GetUser("john"));
            Assert.Empty(messages);
        }

        [Fact]
        public void CreateChannel_ShouldThrowNoErrors()
        {
            Guid clientId = Guid.NewGuid();
            userService.CreateUser("john", clientId);
            notificationServiceMock.Setup((m) => m.GetUserNicknameForClientId(clientId)).Returns("john");
            messageService.CreateChannel("react", "john", clientId);
        }

        [Fact]
        public void CreateChannel_ShouldThrowIfClientIdMismatch()
        {
            Guid clientId = Guid.NewGuid();
            userService.CreateUser("john", clientId);
            Assert.Throws<Exception>(() => messageService.CreateChannel("john", "react", Guid.NewGuid()));
        }

        [Fact]
        public void CreateChannel_ShouldThrowIfChannelAlreadyCreated()
        {
            Guid clientId = Guid.NewGuid();
            userService.CreateUser("john", clientId);
            notificationServiceMock.Setup((m) => m.GetUserNicknameForClientId(clientId)).Returns("john");
            messageService.CreateChannel("react", "john", clientId);
            Assert.Throws<Exception>(() => messageService.CreateChannel("john", "react", clientId));
        }

        [Fact]
        public void CreateChannel_ShouldThrowIfUserNotCreated()
        {
            Guid clientId = Guid.NewGuid();
            Assert.Throws<Exception>(() => messageService.CreateChannel("john", "react", clientId));
        }

        [Fact]
        public void SendMessageToChannel_ShouldCallPublish()
        {
            Guid clientId = Guid.NewGuid();
            userService.CreateUser("john", clientId);
            notificationServiceMock.Setup((m) => m.GetUserNicknameForClientId(clientId)).Returns("john");
            messageService.CreateChannel("react", "john", clientId);
            messageService.SendMessageToChannel("react", "text", "john", clientId);
            notificationServiceMock.Verify(m => m.Publish(It.Is<Message>(
                (m) => m.Channel.Name == "react" &&
                m.Notification.Text == "text" &&
                m.Notification.Author.Nickname == "john")));
        }

        [Fact]
        public void SendMessageToChannel_ShouldThrowIfChannelNotCreated()
        {
            Guid clientId = Guid.NewGuid();
            userService.CreateUser("john", clientId);
            notificationServiceMock.Setup((m) => m.GetUserNicknameForClientId(clientId)).Returns("john");
            Assert.Throws<Exception>(
                () => messageService.SendMessageToChannel("react", "text", "john", clientId)
            );
        }

        [Fact]
        public void SendMessageToChannel_ShouldSendMessageToRecipient()
        {
            Guid clientId = Guid.NewGuid();
            userService.CreateUser("john", clientId);
            Guid bobId = Guid.NewGuid();
            userService.CreateUser("bob", bobId);
            var bob = userService.GetUser("bob");
            notificationServiceMock.Setup((m) => m.GetUserNicknameForClientId(clientId)).Returns("john");
            notificationServiceMock.Setup((m) => m.GetUserNicknameForClientId(bobId)).Returns("bob");
            messageService.CreateChannel("react", "john", clientId);
            messageService.JoinChannel("react", "bob", bobId);
            messageService.SendMessageToChannel("react", "text", "john", clientId);
            notificationServiceMock.Verify(
                m => m.Publish(
                    It.Is<Message>((m) => m.Channel.Recipients.Contains(bob))));
        }

        [Fact]
        public void SendMessageToChannel_ShouldThrowIfUserIsNotRecipient()
        {
            Guid clientId = Guid.NewGuid();
            userService.CreateUser("john", clientId);
            Guid bobId = Guid.NewGuid();
            userService.CreateUser("bob", bobId);
            var bob = userService.GetUser("bob");
            notificationServiceMock.Setup((m) => m.GetUserNicknameForClientId(clientId)).Returns("john");
            notificationServiceMock.Setup((m) => m.GetUserNicknameForClientId(bobId)).Returns("bob");
            messageService.CreateChannel("react", "john", clientId);
            Assert.Throws<Exception>(() => messageService.SendMessageToChannel("react", "text", "bob", bobId));
        }
    }
}
