﻿using Domain.Model;
using Domain.Services.NotificationService;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Domain.Test
{
    public class NotificationServiceTest
    {
        private readonly INotificationService notificationService;

        public NotificationServiceTest()
        {
            this.notificationService = new NotificationService();
        }

        [Fact]
        public void Publish_ShouldSendMessageOnlyToRecipients()
        {
            var guid = Guid.NewGuid();
            var numberOfCallsForJohn = 0;
            notificationService.RegisterListener(guid, (notification) =>
            {
                numberOfCallsForJohn++;
                return Task.CompletedTask;
            });
            var john = new User()
            {
                Nickname = "john",
            };
            var bobGuid = Guid.NewGuid();
            var bob = new User()
            {
                Nickname = "bob"
            };
            var numberOfCallsForBob = 0;
            notificationService.RegisterListener(bobGuid, (notification) =>
            {
                numberOfCallsForBob++;
                return Task.CompletedTask;
            });
            notificationService.AttachListenerToUser(john, guid);
            notificationService.AttachListenerToUser(bob, bobGuid);
            notificationService.Publish(new Message()
            {
                Channel = new Channel()
                {
                    Recipients = new User[] { john }
                }
            });
            Assert.Equal(2, numberOfCallsForJohn);
            Assert.Equal(1, numberOfCallsForBob);
        }

        [Fact]
        public void Publish_ShouldSendMessageToUser()
        {
            var guid = Guid.NewGuid();
            var numberOfCalls = 0;
            notificationService.RegisterListener(guid, (notification) =>
            {
                numberOfCalls++;
                return Task.CompletedTask;
            });
            var user = new User()
            {
                Nickname = "john",
            };
            notificationService.AttachListenerToUser(user, guid);
            notificationService.Publish(new Message()
            {
                Channel = new Channel()
                {
                    Recipients = new User[] { user }
                }
            });
            Assert.Equal(2, numberOfCalls);
        }

        [Fact]
        public void RemoveListenerForUser_ShouldRemoveListenerFromList()
        {
            var guid = Guid.NewGuid();
            notificationService.RegisterListener(guid, (notification) => Task.CompletedTask);
            var user = new User()
            {
                Nickname = "john",
            };
            notificationService.AttachListenerToUser(user, guid);
            notificationService.RemoveUserListeners(user);
            Assert.Empty(notificationService.GetListeners());
        }

        [Fact]
        public void RemoveListenerForUser_ShouldReturnNullIfListenerNotRegistered()
        {
            var guid = Guid.NewGuid();
            var user = new User()
            {
                Nickname = "john",
            };
            Assert.Throws<Exception>(() => notificationService.RemoveUserListeners(user));
        }

        [Fact]
        public void RemoveListenerForUser_ShouldNotifyThatUserDisconnected()
        {
            var guid = Guid.NewGuid();
            var user = new User()
            {
                Nickname = "john",
            };
            var numberOfCalls = 0;
            notificationService.RegisterListener(guid, (notification) =>
            {
                if (notification.Type == Notification.NotificationType.LISTENER_REMOVED) numberOfCalls++;
                return Task.CompletedTask;
            });
            notificationService.AttachListenerToUser(user, guid);
            notificationService.RemoveUserListeners(user);
            Assert.Equal(1, numberOfCalls);
        }

        [Fact]
        public void RemoveListenerForUser_ShouldThrowIfUserNotAttached()
        {
            var guid = Guid.NewGuid();
            var user = new User()
            {
                Nickname = "john",
            };
            notificationService.RegisterListener(guid, (notification) => Task.CompletedTask);

            Assert.Throws<Exception>(() => notificationService.RemoveUserListeners(user));
        }

        [Fact]
        public void GetListenerForUser_ShouldReturnNullIfListenerNotFound()
        {
            var user = new User()
            {
                Nickname = "john",
            };
            Assert.Null(notificationService.GetListenerForUser(user));
        }

        [Fact]
        public void GetUserNicknameForClientId_ShouldReturnNullIfUserNotFound()
        {
            var guid = Guid.NewGuid();
            notificationService.RegisterListener(guid, (notification) => Task.CompletedTask);
            Assert.Null(notificationService.GetUserNicknameForClientId(guid));
        }

        [Fact]
        public void GetUserNicknameForClientId_ShouldThrowIfListenerNotRegistered()
        {
            var guid = Guid.NewGuid();
            Assert.Throws<Exception>(() => notificationService.GetUserNicknameForClientId(guid));
        }

        [Fact]
        public void GetUserNicknameForClientId_ShouldReturnRightNickname()
        {
            var guid = Guid.NewGuid();
            var user = new User()
            {
                Nickname = "john",
            };
            notificationService.RegisterListener(guid, (notification) => Task.CompletedTask);
            notificationService.AttachListenerToUser(user, guid);
            Assert.Equal("john", notificationService.GetUserNicknameForClientId(guid));
        }

        [Fact]
        public void SendError_ShouldCallPublish()
        {
            var guid = Guid.NewGuid();
            var numOfCalls = 0;
            notificationService.RegisterListener(guid, (notification) =>
            {
                numOfCalls++;
                return Task.CompletedTask;
            });
            notificationService.SendError(new Exception("Message"), guid);
            Assert.Equal(1, numOfCalls);
        }

        [Fact]
        public void AttachListenerToUser_ShouldThrowIfUserAlreadyListening()
        {
            var guid = Guid.NewGuid();
            var user = new User()
            {
                Nickname = "john",
            };
            notificationService.RegisterListener(guid, (notification) => Task.CompletedTask);
            notificationService.AttachListenerToUser(user, guid);
            Assert.Throws<Exception>(() => notificationService.AttachListenerToUser(user, guid));
        }
    }
}
