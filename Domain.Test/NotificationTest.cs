﻿using Domain.Model;
using Xunit;

namespace Domain.Test
{
    public class NotificationTest
    {
        [Fact]
        public void Private_ShouldReturnFalseIfDestinationNotSet()
        {
            Assert.False(new Notification().Private);
        }

        [Fact]
        public void Private_ShouldReturnFalseIfDestinationSetAsNull()
        {
            Assert.False(new Notification() { Destination = null }.Private);
        }

        [Fact]
        public void Private_ShouldReturnTrueIfDestinationSet()
        {
            Assert.True(new Notification() { Destination = new User() { Nickname = "john" } }.Private);
        }

        [Fact]
        public void HasError_ShouldReturnTrueIfSet()
        {
            Assert.True(new Notification()
            {
                Destination = new User() { Nickname = "john" },
                Error = "Error!"
            }.HasError);
        }

        [Fact]
        public void HasError_ShouldReturnFalseIfSetNull()
        {
            Assert.False(new Notification()
            {
                Destination = new User() { Nickname = "john" },
                Error = null
            }.HasError);
        }

        [Fact]
        public void HasError_ShouldReturnFalseIfNotSet()
        {
            Assert.False(new Notification()
            {
                Destination = new User() { Nickname = "john" },
            }.HasError);
        }
    }
}
