﻿using Domain.Model;
using Domain.Services.NotificationService;
using Domain.Services.UserService;
using Moq;
using System;
using Xunit;

namespace Domain.Test
{
    public class UserServiceTest
    {
        private readonly IUserService userService;
        private readonly Mock<INotificationService> notificationServiceMock;
        private readonly INotificationService notificationService;

        public UserServiceTest()
        {
            this.notificationServiceMock = new Mock<INotificationService>();
            this.notificationService = notificationServiceMock.Object;
            this.userService = new UserService(this.notificationService);
        }

        [Fact]
        public void CreateUserShouldSetNickname()
        {
            var nickname = "Billie";
            var user = userService.CreateUser(nickname, Guid.NewGuid());
            Assert.Equal(user.Nickname, nickname);
        }

        [Fact]
        public void CreateUserShouldCreateOnlyUniqueNicknames()
        {
            var nickname = "Billie";
            userService.CreateUser(nickname, Guid.NewGuid());
            Assert.Throws<Exception>(() => userService.CreateUser(nickname, Guid.NewGuid()));
        }

        [Fact]
        public void RemoveUser_ShouldRemovesUserFromList()
        {
            var nickname = "Billie";
            Guid clientId = Guid.NewGuid();
            userService.CreateUser(nickname, clientId);
            SetupGetUserNicknameForClientId(nickname, clientId);
            userService.RemoveUser(nickname, clientId);
            Assert.Empty(userService.GetAllUsers());
        }

        [Fact]
        public void RemoveUser_ShouldRemoveListenerUserAssociation()
        {
            var nickname = "Billie";
            Guid guid = Guid.NewGuid();
            var user = userService.CreateUser(nickname, guid);
            SetupGetUserNicknameForClientId(nickname, guid);
            userService.RemoveUser(nickname, guid);
            notificationServiceMock.Verify(mock => mock.RemoveUserListeners(It.IsAny<User>()));
        }

        [Fact]
        public void RemoveUser_ShouldThrowIfClientMismatch()
        {
            var nickname = "Billie";
            Guid guid = Guid.NewGuid();
            var user = userService.CreateUser(nickname, guid);
            SetupGetUserNicknameForClientId(nickname, Guid.NewGuid());
            Assert.Throws<Exception>(() => userService.RemoveUser(nickname, guid));
        }

        [Fact]
        public void RemoveUser_ShouldThrowIfNicknameNull()
        {
            var nickname = "Billie";
            Guid guid = Guid.NewGuid();
            var user = userService.CreateUser(nickname, guid);
            SetupGetUserNicknameForClientId(null, Guid.NewGuid());
            Assert.Throws<Exception>(() => userService.RemoveUser(nickname, guid));
        }

        private void SetupGetUserNicknameForClientId(string nickname, Guid guid)
        {
            notificationServiceMock.Setup((m) => m.GetUserNicknameForClientId(guid)).Returns(nickname);
        }

        [Fact]
        public void GetUser_ShouldFindRightUser()
        {
            var nickname = "Billie";
            userService.CreateUser(nickname, Guid.NewGuid());
            userService.CreateUser("bob", Guid.NewGuid());
            Assert.Equal(nickname, userService.GetUser(nickname).Nickname);
        }

        [Fact]
        public void GetUser_ShouldThrowIfUserNotCreated()
        {
            var nickname = "Billie";
            Assert.Throws<Exception>(() => userService.GetUser(nickname).Nickname);
        }

        [Fact]
        public void GetUser_ShouldThrowIfNicknameIsBlank()
        {
            var nickname = "";
            Assert.Throws<Exception>(() => userService.CreateUser(nickname, Guid.NewGuid()));
        }
    }
}
