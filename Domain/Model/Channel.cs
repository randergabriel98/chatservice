﻿using System.Collections.Generic;
using System.Linq;

namespace Domain.Model
{
    public class Channel
    {
        public string Name { get; set; }
        public User Creator { get; set; }
        public ICollection<User> Recipients { get; set; }
        public bool IsPublic { get; set; }

        public bool UserIsInChannel(User user)
        {
            return IsPublic || Recipients.Contains(user);
        }
    }
}