﻿using System;
using System.Threading.Tasks;

namespace Domain.Model
{
    public class Listener
    {
        public User User { get; set; }
        public Guid ClientId { get; set; }
        //TODO: Encontrar nome menos genérico
        public Func<Notification, Task> Publish { get; set; }
    }
}
