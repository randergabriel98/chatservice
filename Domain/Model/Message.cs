﻿namespace Domain.Model
{
    public class Message
    {
        public Notification Notification { get; set; }
        public Channel Channel { get; set; }
    }
}
