﻿namespace Domain.Model
{
    public class Notification
    {
        public NotificationType Type { get; set; } = NotificationType.TEXT_MESSAGE;
        public User Author { get; set; }
        public string Error { get; set; }
        public bool HasError => Error != null;
        public string Text { get; set; }
        public string Channel { get; set; }
        //TODO: Melhorar tratamento de Notification/Command
        public User Destination { get; set; }
        public bool Private => Destination != null;

        public enum NotificationType
        {
            TEXT_MESSAGE,
            USER_CREATED,
            LISTENER_REMOVED
        }
    }
}
