﻿using Domain.Model;
using System;
using System.Collections.Generic;

namespace Domain.Services.MessageService
{
    public interface IMessageService
    {
        void SendPublicMessage(User sender, string text);
        void SendPrivateMessage(User sender, string text, User receiver);
        void SendMessage(string senderNickname, string text, Guid clientId, string receiverNickname, string channel);
        IEnumerable<Message> GetAllMessages();
        IEnumerable<Message> GetMessagesForUser(User receiver);
        void JoinChannel(string channelName, string userNickname, Guid clientId);
        void CreateChannel(string channelName, string creatorNickname, Guid clientId);
        void SendMessageToChannel(string channelName, string messageText, string userNickname, Guid clientId);
    }
}
