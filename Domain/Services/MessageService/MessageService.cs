﻿using Domain.Model;
using Domain.Services.NotificationService;
using Domain.Services.UserService;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Services.MessageService
{
    /**
     * <summary>Classe <c>MessageService</c> gerencia
     * e direciona Mensagens da aplicação</summary>
     */
    public class MessageService : IMessageService
    {
        private readonly INotificationService notificationService;
        private readonly IUserService userService;
        //TODO: Mover para repository layer
        private readonly ICollection<Message> messages = new List<Message>();
        private readonly ICollection<Channel> openChannels = new List<Channel>();

        public MessageService(INotificationService notificationService, IUserService userService)
        {
            this.notificationService = notificationService;
            this.userService = userService;
        }

        public IEnumerable<Message> GetAllMessages()
        {
            return this.messages;
        }

        public void SendPrivateMessage(User sender, string text, User receiver)
        {
            var channel = CreatePrivateChannel(receiver);
            var message = new Message()
            {
                Notification = new Notification()
                {
                    Author = sender,
                    Text = text,
                    Destination = receiver
                },
                Channel = channel,
            };
            SendMessage(message);
        }

        private void SendMessage(Message message)
        {
            messages.Add(message);
            notificationService.Publish(message);
        }

        private static Channel CreatePrivateChannel(User receiver)
        {
            var channel = new Channel()
            {
                Name = $"{receiver.Nickname}'s private channel",
                Recipients = new List<User>() { receiver },
                IsPublic = false
            };
            return channel;
        }

        public void SendPublicMessage(User sender, string text)
        {
            var channel = new Channel()
            {
                Name = "Public",
                IsPublic = true,
                Recipients = userService.GetAllUsers()
            };
            var message = new Message()
            {
                Notification = new Notification()
                {
                    Author = sender,
                    Text = text
                },
                Channel = channel,
            };

            SendMessage(message);
        }

        public IEnumerable<Message> GetMessagesForUser(User receiver)
        {
            return this.messages.Where(m => m.Channel.UserIsInChannel(receiver));
        }

        public void SendMessage(string senderNickname, string text, Guid clientId, string receiverNickname, string channelName)
        {
            userService.ValidateUser(senderNickname, clientId);
            if(channelName != null)
            {
                this.SendMessageToChannel(channelName, text, senderNickname, clientId);
                return;
            }
            var sender = userService.GetUser(senderNickname);
            if (receiverNickname != null)
            {
                SendPrivateMessage(sender, text, userService.GetUser(receiverNickname));
                return;
            }
            SendPublicMessage(sender, text);
        }

        public void SendMessageToChannel(string channelName, string messageText, string userNickname, Guid clientId)
        {
            userService.ValidateUser(userNickname, clientId);
            var sender = userService.GetUser(userNickname);
            var channel = GetChannel(channelName);
            if(!channel.Recipients.Contains(sender))
            {
                throw new Exception("User is not registered in channel");
            }
            var message = new Message()
            {
                Channel = channel,
                Notification = new Notification()
                {
                    Author = sender,
                    Text = messageText,
                    Channel = channelName
                },
            };
            SendMessage(message);
        }

        public void CreateChannel(string channelName, string userNickname, Guid clientId)
        {
            userService.ValidateUser(userNickname, clientId);
            var creator = userService.GetUser(userNickname);
            var channel = new Channel()
            {
                Name = channelName,
                IsPublic = false,
                Creator = creator,
                Recipients = new List<User>() { creator }
            };
            if(openChannels.Any(item => item.Name == channelName)) {
                throw new Exception("Unable to create channel. Another channel with the same name already exists");
            }
            RemovePreviousChannel(creator);
            this.openChannels.Add(channel);
        }

        public void JoinChannel(string channelName, string userNickname, Guid clientId)
        {
            userService.ValidateUser(userNickname, clientId);
            User user = userService.GetUser(userNickname);
            RemovePreviousChannel(user);
            Channel channel = GetChannel(channelName);
            channel.Recipients.Add(user);
        }

        private void RemovePreviousChannel(User user)
        {
            var existingChannel = openChannels.SingleOrDefault(c => c.Recipients?.Contains(user) ?? false);
            if (existingChannel != null)
            {
                existingChannel.Recipients.Remove(user);
            }
        }

        private Channel GetChannel(string channelName)
        {
            Channel channel = this.openChannels.SingleOrDefault(c => c.Name == channelName);
            if (channel == null)
            {
                throw new Exception($"Channel '{channelName}' does not exist");
            }

            return channel;
        }
    }
}
