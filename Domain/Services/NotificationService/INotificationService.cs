﻿using Domain.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Services.NotificationService
{
    public interface INotificationService
    {
        void RegisterListener(Guid clientId, Func<Notification, Task> subscription);
        void AttachListenerToUser(User user, Guid clientId);
        void RemoveUserListeners(User user);
        void Publish(Message message);
        IEnumerable<Listener> GetListeners();
        string GetUserNicknameForClientId(Guid clientId);
        Listener GetListenerForUser(User recipient);
        void SendError(Exception ex, Guid clientId);
    }
}
