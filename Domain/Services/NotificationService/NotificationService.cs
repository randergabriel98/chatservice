﻿using Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Services.NotificationService
{
    public class NotificationService : INotificationService
    {
        private ICollection<Listener> listeners = new List<Listener>();

        public void AttachListenerToUser(User user, Guid clientId)
        {
            var userHasListener = GetListenerForUser(user) != null;
            if (userHasListener)
            {
                throw new Exception("Forbidden! User already registered.");
            }
            var listener = listeners.Single(l => l.ClientId == clientId);
            listener.User = user;
            listener.Publish(new Notification() { Type = Notification.NotificationType.USER_CREATED });
        }

        public void Publish(Message message)
        {
            foreach (var recipient in message.Channel.Recipients)
            {
                Listener listener = GetListenerForUser(recipient);
                listener.Publish(message.Notification);
            }
        }

        public Listener GetListenerForUser(User recipient)
        {
            return this.listeners.Where(l => l.User?.Nickname == recipient.Nickname).SingleOrDefault();
        }

        public void RegisterListener(Guid clientId, Func<Notification, Task> subscription)
        {
            listeners.Add(new Listener()
            {
                ClientId = clientId,
                Publish = subscription,
            });
        }

        public void RemoveUserListeners(User user)
        {
            //TODO: Forçar disconnect/dispose
            Listener item = GetListenerForUser(user);
            if (item == null)
            {
                throw new Exception("Listener not found");
            }
            item.Publish(new Notification() { Type = Notification.NotificationType.LISTENER_REMOVED });
            this.listeners.Remove(item);
        }

        public IEnumerable<Listener> GetListeners()
        {
            return this.listeners;
        }

        public string GetUserNicknameForClientId(Guid clientId)
        {
            Listener listener = GetListener(clientId);
            if (listener == null) throw new Exception("Listener no found");
            return listener.User?.Nickname;
        }

        private Listener GetListener(Guid clientId)
        {
            return GetListeners().Where(l => l.ClientId == clientId).SingleOrDefault();
        }

        public void SendError(Exception ex, Guid clientId)
        {
            GetListener(clientId).Publish(new Notification() { Error = ex.Message });
        }
    }
}
