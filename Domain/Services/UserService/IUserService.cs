﻿using Domain.Model;
using System;
using System.Collections.Generic;

namespace Domain.Services.UserService
{
    public interface IUserService
    {
        User CreateUser(string nickname, Guid clientId);
        ICollection<User> GetAllUsers();
        void RemoveUser(string userNickname, Guid clientId);
        User GetUser(string nickname);
        /**
         * <summary>Checks if user with nickname was created with clientId. If validation
         * fails, throws an exception</summary>
         * <param name="nickname">Nickname for user</param>
         * <param name="clientId">Guid for client that sent the message</param>
         */
        void ValidateUser(string nickname, Guid clientId);
    }
}
