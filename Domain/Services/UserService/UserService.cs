﻿using Domain.Model;
using Domain.Services.NotificationService;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Services.UserService
{
    /**
     * <summary>Classe <c>UserService</c> gerencia
     * os usuários da aplicação</summary>
     */
    public class UserService : IUserService
    {
        //TODO: Mover para repository layer
        private ICollection<User> users = new List<User>();
        private readonly INotificationService notificationService;

        public UserService(INotificationService notificationService)
        {
            this.notificationService = notificationService;
        }

        public User CreateUser(string nickname, Guid clientId)
        {
            CheckNickname(nickname);
            var user = new User() { Nickname = nickname };
            AddUser(user);
            notificationService.AttachListenerToUser(user, clientId);
            return user;
        }

        public ICollection<User> GetAllUsers()
        {
            return this.users;
        }

        public User GetUser(string nickname)
        {
            IEnumerable<User> enumerable = this.users.Where(u => u.Nickname == nickname);
            if (enumerable.Count() == 0)
            {
                //TODO: Customizar exceções
                throw new Exception("User doesn't exist");
            }
            return enumerable.First();
        }

        public void RemoveUser(string nickname, Guid clientId)
        {
            this.ValidateUser(nickname, clientId);
            var user = this.GetUser(nickname);
            this.users.Remove(user);
            this.notificationService.RemoveUserListeners(user);
        }

        public void ValidateUser(string nickname, Guid clientId)
        {
            var user = this.GetUser(nickname);
            string userRegistered = this.notificationService.GetUserNicknameForClientId(clientId);
            if (user.Nickname != userRegistered)
            {
                throw new Exception("Client not authorized to make this modification");
            }
        }

        private void AddUser(User user)
        {
            this.users.Add(user);
        }

        private void CheckNickname(string nickname)
        {
            if (users.Where(u => u.Nickname == nickname).Any())
            {
                //TODO: Customizar exceções
                throw new Exception("Nickname already taken, please, choose another one");
            }
            if (string.IsNullOrWhiteSpace(nickname))
            {
                throw new Exception("The nickname cannot be empty");
            }
        }

    }
}
