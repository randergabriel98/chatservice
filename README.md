# ChatService

## Descrição

ChatService é um serviço de bate papo onde usuários podem:

- Se cadastrar com um "nickname" único
- Solicitar ajuda (lista de comandos)
- Enviar mensagns públicas
- Enviar mensagens privadas para outros usuários
- Criar um canal
- Trocar de canal
- Enviar mensagens para um canal
- Sair do chat


## Aplicações

O sistema é composto por duas aplicações, um cliente (CLI) e um servidor (também uma CLI) que trocam mensagens usando o protocolo WebSocket.


## Como executar?

Para executar o sistema, é necessário executar, no mínimo, uma instância do cliente e uma instância do servidor. Para executar, é preciso rodar os dois comandos a seguir na
raiz do projeto, em duas janelas separadas do terminal:

``
dotnet run --project ChatServer/ChatServer.csproj
``


``
dotnet run --project ChatService/ChatService.csproj
``

Vários clientes podem ser executados ao mesmo tempo. Para executar mais de uma instância, basta rodar o comando a partir de várias janelas do terminal.

## Arquitetura

Ambas as aplicações (cliente e servidor) foram implementados usando a linguagem C# e rodam em .NET Core.
O sistema está dividido em projetos dotnet, todos referenciados na solution [ChatService/ChatService.sln](ChatService/ChatService.sln). Os projetos foram definidos conforme sua responsabilidade no
sistema. A decisão de dividir o projeto em camadas foi motivada principalmente para favorecer a testabilidade e a reusabilidade do código da aplicação.
Construir uma nova aplicação que utilize outro protocolo de comunicação entre o cliente e o servidor (HTTP, por exemplo) se torna uma tarefa muito mais fácil, uma vez que 
basta criar uma nova implementação da camada mais externa do servidor ([ChatServer/INetworkServerAdapter](ChatServer/INetworkServerAdapter.cs)), implementando somente a comunicação com os clientes.


## Camadas

- [ChatService](ChatService)
  - CLI usada pelo <b>cliente</b> da aplicação. Ela é responsável por receber e interpretar as mensagens digitadas pelo usuário no console, gerenciar a conexão com o servidor,
  bem como por fazer o tratamento das mensagens enviadas para e e pelo servidor 
  
- [ChatServer](ChatServer)
  - CLI resposável por servir como <b>servidor</b> WebSocket, receber as mensagens enviadas pelos clientes e enviá-las às camadas mais internas da aplicação, bem como direcionar mensagens disparadas pelas camadas mais internas para os respectivos clientes
  
- [Domain](Domain)
  - Biblioteca de Classes .NET Standard responsável pelo gerenciamento de regras de negócio da aplicação, bem como pela recuperação e orquestração dos objetos de domínio.
  Nela são tomadas decisões como:
    - Para quais clientes uma mensagem deve ser enviada?
    - É possível criar um usuário com o apelido "joãozinho"?
 
- [Application](Application)
  - Biblioteca de classes .NET Core que é responsável pela comunicação entre a camada de domínio ([Domain](Domain)) e de servidor ([ChatServer](ChatServer)).
  Nela as entidades de domínio são mapeadas para DTOs. Serve também como forma de desacoplar a lógica de domínio da lógica de gerenciamento de conexões, protocolos de
  rede e outras dependências externas.

- [Serializer](Serializer)
  - Biblioteca de classes .NET Core que serve para centralizar a lógica de serialização e desserialização de objetos usados na comunicação Cliente-Servidor,
  tanto pela camada de [Application](Application) quanto pela camada [ChatService](ChatService). 
  
## Testes
  
O sistema possui quatro projetos de teste. Um para a camada de [Domain](Domain) ([Domain.Test](Domain.Test)), um para a camada de
[Application](Application) ([Application.Test](Application.Test)), um para a camada de [Serializer](Serializer) ([Serializer.Test](Serializer.Test)) e outro para a camada
de [ChatServer](ChatServer) ([ChatServer.Test](ChatServer.Test)). Ambos possuem, em sua maioria, testes unitários, mas também alguns testes de integração.

Para rodar os testes, basta abrir o cmd, navegar até a pasta do projeto de teste e executar o comando:

``dotnet test``

Para rodar os testes e ler o relatório de cobertura de testes de um projeto, basta executar o comando a seguir, na pasta do projeto:

``dotnet test /p:CollectCoverage=true``

Para rodar todos os testes da aplicação e ler o relatório de cobertura geral, basta rodar o seguinte comando, a partir da pasta [ChatService](ChatService):

``dotnet test /p:CollectCoverage=true /p:CoverletOutput="../" /p:MergeWith="../coverage.json" /maxcpucount:1 /p:CoverletOutputFormat=\"json,opencover\"`` 

#### Cobertura de testes

A tabela a seguir mostra a cobertura de testes (calculada com o comando acima, usando a ferramenta coverlet) para o projeto. A cobertura não inclui o projeto 
[ChatService](ChatService), pois não foram implementados testes para a camada do cliente.



| Module      | Line   | Branch | Method |     
|-------------|--------|--------|--------|     
| Application | 94,93% | 82,75% | 100%   |     
| ChatServer  | 29,76% | 31,25% | 25,8%  |     
| Domain      | 96,17% | 87,5%  | 100%   |     
| Serializer  | 80,66% | 96,15% | 86,48% |     

|         | Line   | Branch | Method |
|---------|--------|--------|--------|
| Total   | 75,55% | 84,18% | 77,95% |
| Average | 75,38% | 74,41% | 78,07% |

Algumas camadas do projeto possuem menos testes que outras porque não tive tempo para implementar tantos testes quanto gostaria. Foi dada prioridade às camadas
mais internas da aplicação ([Domain](Domain), [Application](Application)) pois nelas é que são validadas as regras mais críticas e complexas da aplicação, enquanto as
camadas mais externas lidam com integrações com APIs e agentes externos.

## Pendências


O projeto possui algumas pendências técnicas


- Não foram implementados testes para a camada de [ChatService](ChatService)
- Não foram implementados testes E2E
- Não foi implementada persistência de dados (o servidor é stateful, o que impossibilita escalabilidade horizontal)
- O acesso a dados não foi separado em outra camada (Data Access Object ou Repository Pattern)
- O projeto não utiliza ferramentas de <b>Dependency Injection</b>, pois a ideia era implementar o projeto sem utilizar bibliotecas de terceiros no código de produção
- A aplicação do frontend [ChatService](ChatService) não foi implementada com as melhores práticas de desenvolvimento

Algumas dessas pendências não foram tratadas por falta de tempo e outras por não fazerem parte do escopo original do projeto, mas seria muito positivo que fossem incorporadas
ao projeto em algum momento caso ele fosse ser usado em produção.
