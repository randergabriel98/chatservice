using Serializer.DTO;
using System;
using System.Collections.Generic;
using Xunit;

namespace Serializer.Test
{
    public class SerializeHelperTest
    {
        [Theory]
        [InlineData(nameof(SendMessageCommand), typeof(SendMessageCommand))]
        [InlineData(nameof(UserRemovedCommand), typeof(UserRemovedCommand))]
        [InlineData(nameof(ApplicationMessageCommand), typeof(ApplicationMessageCommand))]
        [InlineData(nameof(UserCreatedCommand), typeof(UserCreatedCommand))]
        public void DeserializeCommand_TestType(string typeName, Type expectedClass)
        {
            var obj = SerializeHelper.DeserializeCommand(BuildJsonString(typeName));
            Assert.Equal(obj.GetType(), expectedClass);
        }

        private static string BuildJsonString(string typeName)
        {
            return $"{{\"CommandType\":\"{typeName}\"}}";
        }

        [Fact]
        public void DeserializeCommand_ThrowIfUnknown()
        {
            var typeName = "FakeClass";
            Assert.Throws<Exception>(() => SerializeHelper.DeserializeCommand(BuildJsonString(typeName)));
        }

        [Theory]
        [MemberData(nameof(DataforSerializeCommandTestType))]
        public void SerializeCommand_TestType(ICommandTypeMatcher objToParse)
        {
            // Checando se consegue converter sem erros. Um Assert.Equal aqui me
            // obrigaria a adequar esse teste sempre que modificasse uma classe
            SerializeHelper.SerializeCommand(objToParse);
        }

        [Fact]
        public void SerializeCommand_ShouldThrowIfUnknown()
        {
            Assert.Throws<Exception>(() => SerializeHelper.SerializeCommand(new FakeCommand()));
        }

        public static IEnumerable<object[]> DataforSerializeCommandTestType => new List<object[]>()
                {
                    new object[] {
                        new SendMessageCommand(),
                    },
                    new object[] {
                        new UserRemovedCommand(),
                    },
                    new object[] {
                        new ApplicationMessageCommand(),
                    },
                    new object[] {
                        new ApplicationMessageCommand()
                        {
                            Payload = new ApplicationPayload()
                            {
                                Private = false,
                                Sender = "test",
                                Text = "test"
                            }
                        },
                    },
                    new object[] {
                        new UserCreatedCommand(),
                    },
                    new object[] {
                        new UserCreatedCommand()
                        {
                            Payload = new UserCreatedPayload()
                        },
                    },
                };
    }

    internal class FakeCommand : ICommandTypeMatcher
    {
        public string CommandType => nameof(FakeCommand);
    }
}
