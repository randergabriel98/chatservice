﻿namespace Serializer.DTO
{
    public class ApplicationError
    {
        public int Code { get; set; }
        public string Message { get; set; }
    }
}