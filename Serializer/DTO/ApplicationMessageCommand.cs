﻿namespace Serializer.DTO
{
    public class ApplicationMessageCommand : Command<ApplicationPayload>
    {
        public ApplicationError Error { get; set; }
        public bool Success
        {
            get
            {
                return Error == null;
            }
        }

        public override string CommandType => nameof(ApplicationMessageCommand);
    }

    public class ApplicationPayload
    {
        public string Sender { get; set; }
        public string Text { get; set; }
        public bool Private { get; set; }
        public string Channel { get; set; }
    }
}
