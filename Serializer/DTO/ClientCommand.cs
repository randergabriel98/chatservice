﻿namespace Serializer.DTO
{
    public class ClientCommand
    {
        public SendMessageCommand Command { get; set; }
        public string Nickname { get; set; }
    }
}
