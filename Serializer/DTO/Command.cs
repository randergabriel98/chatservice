﻿namespace Serializer.DTO
{
    public abstract class Command<T> : ICommandTypeMatcher
    {
        public T Payload { get; set; }
        public abstract string CommandType { get; }
    }

    public interface ICommandTypeMatcher
    {
        public string CommandType { get; }
    }
}
