﻿namespace Serializer.DTO
{
    public class SendMessageCommand : Command<SendMessagePayload>
    {
        public bool Public
        {
            get
            {
                return Payload?.Destination == null;
            }
        }
        public MessageAction Action { get; set; }

        public override string CommandType => nameof(SendMessageCommand);
    }

    //TODO: Encontrar um nome melhor
    public class SendMessagePayload
    {
        public string Destination { get; set; }
        public string Text { get; set; }
        public string Channel { get; set; }
    }

    public enum MessageAction
    {
        SEND_TEXT,
        CREATE_USER,
        EXIT,
        CREATE_CHANNEL,
        JOIN_CHANNEL,
    }
}
