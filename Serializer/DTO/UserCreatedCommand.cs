﻿namespace Serializer.DTO
{
    public class UserCreatedCommand : Command<UserCreatedPayload>
    {
        public override string CommandType => nameof(UserCreatedCommand);
    }
    public class UserCreatedPayload
    {
        public bool UserCreated { get => true; }
    }
}
