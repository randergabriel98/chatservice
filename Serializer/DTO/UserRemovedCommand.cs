﻿namespace Serializer.DTO
{
    public class UserRemovedCommand : Command<UserRemovedPayload>
    {
        public override string CommandType => nameof(UserRemovedCommand);
    }

    public class UserRemovedPayload
    {
    }
}
