﻿using Serializer.DTO;
using System;
using System.Text;
using System.Text.Json;

namespace Serializer
{
    public static class SerializeHelper
    {
        public static byte[] SerializeBytes<T>(T cmd) => JsonSerializer.SerializeToUtf8Bytes(cmd);
        public static T DeserializeString<T>(string rawCmd)
        {
            return JsonSerializer.Deserialize<T>(rawCmd, new JsonSerializerOptions()
            {
                PropertyNameCaseInsensitive = true,
            });
        }

        private static string BytesToString(ArraySegment<byte> bytes, int messageSize)
        {
            return Encoding.UTF8.GetString(bytes.ToArray(), 0, messageSize);
        }

        public static ICommandTypeMatcher DeserializeCommand(string rawRes)
        {
            var type = DeserializeString<TypeMatcher>(rawRes).CommandType;
            return type switch
            {
                nameof(SendMessageCommand) => DeserializeString<SendMessageCommand>(rawRes),
                nameof(ApplicationMessageCommand) => DeserializeString<ApplicationMessageCommand>(rawRes),
                nameof(UserCreatedCommand) => DeserializeString<UserCreatedCommand>(rawRes),
                nameof(UserRemovedCommand) => DeserializeString<UserRemovedCommand>(rawRes),
                _ => throw new Exception($"Parse error. Type {type} uknown"),
            };
        }

        public static ICommandTypeMatcher DeserializeCommand(ArraySegment<byte> bytesReceived, int count)
        {
            return DeserializeCommand(BytesToString(bytesReceived, count));
        }

        public static string SerializeCommand(ICommandTypeMatcher command)
        {
            switch (command.CommandType)
            {
                case nameof(SendMessageCommand):
                    return SerializeString<SendMessageCommand>(command as SendMessageCommand);
                case nameof(ApplicationMessageCommand):
                    return SerializeString<ApplicationMessageCommand>(command as ApplicationMessageCommand);
                case nameof(UserCreatedCommand):
                    return SerializeString<UserCreatedCommand>(command as UserCreatedCommand);
                case nameof(UserRemovedCommand):
                    return SerializeString<UserRemovedCommand>(command as UserRemovedCommand);
                default:
                    throw new Exception($"Parse error. Type {command.CommandType} uknown");
            }
        }

        public static string SerializeString<T>(T userCreatedCommand)
        {
            //TODO: Tratar mensagens maiores que 1024 bytes
            byte[] bytes = SerializeBytes(userCreatedCommand);
            return BytesToString(bytes, bytes.Length);
        }
    }
}
